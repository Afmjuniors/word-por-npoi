﻿using System;
using System.Collections.Generic;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using nopi_word.Models;
using System.Globalization;
using NPOI.XWPF.UserModel;

namespace nopi_word.NopiWord
{
    public class DocumentProcessor
    {
        // Função para importar o gráfico para o documento Word
        public static void ImportarGraficoParaWord(string excelFilePath, string wordFilePath)
        {
            // O código para importar o gráfico para o Word permanece o mesmo...
        }

        // Função para adicionar todos os dados do CSV na planilha "inputRawData"
        public static void AdicionarDadosDoCSV(string filePath, string csvFilePath)
        {
            bool isXlsx = Path.GetExtension(filePath).Equals(".xlsx", StringComparison.OrdinalIgnoreCase);

            using (var excelFs = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite))
            {
                IWorkbook workbook;
                if (isXlsx)
                    workbook = new XSSFWorkbook(excelFs); // Para arquivos XLSX (Excel 2007 e posterior)
                else
                    workbook = new HSSFWorkbook(excelFs); // Para arquivos XLS (Excel 97-2003)

                // Obter a planilha "inputRawData" que contém a tabela de dados
                ISheet worksheetDados = workbook.GetSheet("inputRawData");

                // Ler os dados do arquivo CSV e adicionar na planilha do Excel
                using (var reader = new StreamReader(csvFilePath))
                {
                    int linhaInicial = 1; // Supondo que a tabela de dados começa na linha 2
                    while (!reader.EndOfStream)
                    {
                        string linha = reader.ReadLine();
                        string[] valores = linha.Split(',');

                        // Converter os valores para os tipos de dados corretos
                        DateTime data = DateTime.Parse(valores[0]);
                        int linhaCelula = int.Parse(valores[1]);
                        double temperatura = double.Parse(valores[2]);
                        double umidade = double.Parse(valores[3]);
                        double pressao = double.Parse(valores[4]);

                        // Criar uma nova linha na planilha e adicionar os dados
                        IRow row = worksheetDados.CreateRow(linhaInicial);

                        row.CreateCell(0).SetCellValue(data);
                        row.CreateCell(1).SetCellValue(linhaCelula);
                        row.CreateCell(2).SetCellValue(temperatura);
                        row.CreateCell(3).SetCellValue(umidade);
                        row.CreateCell(4).SetCellValue(pressao);

                        linhaInicial++;
                    }
                }

                // Salvar o arquivo Excel atualizado
                workbook.Write(excelFs);
            }
        }

        // Função para ler o arquivo CSV e retornar uma lista de objetos contendo as informações
        public static List<Dado> LerDadosCSV(string filePath)
        {
            List<Dado> dados = new List<Dado>();

            // Verificar se o arquivo existe
            if (File.Exists(filePath))
            {
                using (var reader = new StreamReader(filePath))
                {
                    // Ler o cabeçalho (opcional, dependendo do formato do arquivo CSV)
                    // Exemplo de cabeçalho: "Data,Valor"
                    string header = reader.ReadLine();

                    // Ler os dados linha por linha e adicionar à lista
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        var values = line.Split(',');
                        if (values.Length >= 2)
                        {
                            DateTime data;
                            double valor;

                            if (DateTime.TryParse(values[0], out data) && double.TryParse(values[1], NumberStyles.Any, CultureInfo.InvariantCulture, out valor))
                            {
                                dados.Add(new Dado { Data = data, Valor = valor });
                            }
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Arquivo CSV não encontrado: " + filePath);
            }

            return dados;
        }

        public static void CriarArquivoDocComDados(Alarme dados)
        {
            // Criar um novo documento .doc
            XWPFDocument document = new XWPFDocument();
            // Adicionar as informações no documento
            // Por exemplo, você pode criar uma tabela para representar os dados
            XWPFTable table = document.CreateTable(dados.Pressao.Values.Count + 1, 4);
            table.GetRow(0).GetCell(0).SetText("ID");
            table.GetRow(0).GetCell(1).SetText("Sensor");
            table.GetRow(0).GetCell(2).SetText("Tipo");
            table.GetRow(0).GetCell(3).SetText("Impacto");
            int rowIndex = 1;
            foreach (var pressaoItem in dados.Pressao.Values.SelectMany(p => p.b))
            {
                // Verificar se o item não é nulo antes de acessar suas propriedades
                if (pressaoItem != null)
                {
                    table.GetRow(rowIndex).GetCell(0).SetText(pressaoItem.id != null ? pressaoItem.id.ToString() : "");
                    table.GetRow(rowIndex).GetCell(1).SetText(pressaoItem.sensor != null ? pressaoItem.sensor.ToString() : "");
                    table.GetRow(rowIndex).GetCell(2).SetText(pressaoItem.tipo != null ? pressaoItem.tipo.ToString() : "");
                    table.GetRow(rowIndex).GetCell(3).SetText(pressaoItem.impacto.ToString());
                }
                else
                {
                    // Caso o item seja nulo, podemos tratar a tabela conforme necessário
                    // Por exemplo, você pode preencher células com valores padrão ou vazios.
                    table.GetRow(rowIndex).GetCell(0).SetText("");
                    table.GetRow(rowIndex).GetCell(1).SetText("");
                    table.GetRow(rowIndex).GetCell(2).SetText("");
                    table.GetRow(rowIndex).GetCell(3).SetText("");
                }

                rowIndex++;
            }
            // Salvar o documento em um arquivo .doc
            string pathNewDoc = "C:\\Users\\Homeoffice\\Source\\Repos\\word-por-npoi\\nopi-word\\nopi-word\\Arquivos\\";
            using (FileStream fs = new FileStream(pathNewDoc + "relatorio.doc", FileMode.Create, FileAccess.Write))
            {
                document.Write(fs);
            }
            Console.WriteLine("Relatório criado com sucesso!");
        }
        
    }
}
