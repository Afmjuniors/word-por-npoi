﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace nopi_word.Models.temperatura
{
    public class TempData
    {
        [JsonProperty("02.G.137")]
        public Dictionary<string, List<DataItemBase>> Values { get; set; }
    }
}
