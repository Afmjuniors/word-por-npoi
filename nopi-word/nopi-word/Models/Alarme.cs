﻿using nopi_word.Models.humidade;
using nopi_word.Models.pressao;
using nopi_word.Models.temperatura;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace nopi_word.Models
{
    public class Alarme
    {
        [JsonProperty("data_inicial")]
        public DateTime DataInicial { get; set; }
        [JsonProperty("data_final")]
        public DateTime DataFinal { get; set; }
        [JsonProperty("temp")]
        public Dictionary<string, TempDataItem> Temperatura { get; set; }
        [JsonProperty("umidade")]
        public Dictionary<string, HumidadeDataItem> Umidade { get; set; }
        [JsonProperty("pressao")]
        public Dictionary<string, PressaoDataItem> Pressao { get; set; }
    }
}
