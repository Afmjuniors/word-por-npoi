﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nopi_word.Models
{
    public class DataItemBase
    {
        public string id { get; set; }
        public string sensor { get; set; }
        public string tipo { get; set; }
        public bool impacto { get; set; }
    }
}
