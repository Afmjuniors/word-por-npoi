﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nopi_word.Models.pressao
{
    public class PressaoData
    {
        [JsonProperty("02.G.137")]
        public Dictionary<string, List<PressaoDataItem>> Values { get; set; }
    }
}
